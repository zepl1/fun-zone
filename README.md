# Fun Zone!
start `zepl-broker` in this directory in seperate terminal.

configure your board(s) (cfg-<blabla>.json) then edit
add.py to use your config and execute it to add your boards
on the broker

you can then use `zepl-client` to:
```
# synchronize files (broker->dev)
zepl-client dev sync kk

# run app on device
zepl-client dev run kk 4

# listen to log output
zepl-client log
```
or try the jupyter notebook version (recommended!)

### dir structure
```
```
### zepl config
### board config