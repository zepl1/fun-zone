import json, sys, os, binascii, gc, hashlib, select
import webrepl

from board import Board

import time

board = Board()
board.wifi_connect()

webrepl.start()

# initialization
p = select.poll()
p.register(sys.stdin, select.POLLIN)

board.update_hashtab()

print('Waiting for input...')
board.set_led((9,0,9))

act=[]
ctrl_input=None
ledflag = False
while True:
    while p.poll(100):
        ctrl_input = sys.stdin.read(1)
    #print('CTRL_INPUT {}'.format(ctrl_input))

    if ctrl_input == '1':
        print('DEV_MAC {}'.format(board.dev_mac))
    elif ctrl_input == '2':
        print('F_HTAB {}'.format(board.f_hashtab))
    elif ctrl_input == '3':
        #print('HEART BEAT')
        print('LOG sleeping 1 seconds')
        time.sleep(1)
        if ledflag:
            color = (7,7,7)
        else:
            color = (0,0,0)
        board.set_led(color)
        ledflag = not(ledflag)
    elif ctrl_input == '4':
        import sol_bat
    elif ctrl_input == '5':
        import selftest
    elif ctrl_input == '0':
        import zero
    else:
        if ctrl_input:
            print('ERROR unknown control input: {}'.format(ctrl_input))
