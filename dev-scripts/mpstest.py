from board import Board
from mp8862 import MP8862
import json

from time import sleep

board = Board()

m = MP8862(board.i2c, board.pins['mp_en'], v_max=2.9, i_max=0.5)

def ramp_down():
    # 'linspace' 1.5 V -> 0 V  in 10mV steps
    ramp = [x*0.01 for x in range(150,-1,-1)]
    for x in ramp:
        m.set_voltage(x)
        sleep(0.1)


m.set_voltage(2.9)
m.set_ilim(0.5)
m.enable()

m.update_status()
print(m.status)


