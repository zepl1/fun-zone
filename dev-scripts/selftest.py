from machine import Pin, TouchPad, I2C, SDCard
#from machine import Pin, TouchPad, I2C, I2S, SDCard
from kk import KK
import os
import json

from time import sleep

board = KK()

# check if i2c devices are present
scan = board.i2c.scan()
for dev in board.devices.keys():
    if board.devices[dev]['addr'] in scan:
        board.devices[dev]['present'] = True
    else:
        board.devices[dev]['present'] = False
for dev in board.devices.keys():
    print("LOG I2C Device {} present -> {}".format(dev, board.devices[dev]['present']))


if board.sdcard:
    success = True
    try:
        os.mount(SDCard(), "/sd")
        board.set_led((0,15,0))
        #print('LOG SD Card mounted.')
    except:
        board.set_led((15,0,0))
        #print('ERROR Could not Mount SD Card!')
        success = False

    print('LOG SD Card: Present ? {}'.format(success))
