#! /usr/bin/env python3
import zmq, json
from zepl_broker import WebReplClient
from time import sleep
from pprint import pprint

cfile = 'cfg-kk.json'
broker_ip = "127.0.0.1"

with open(cfile, 'r') as f:
    cfg = json.loads(f.read())

ctx = zmq.Context()
client = WebReplClient(ctx, broker_ip)
for dev in cfg.keys():
    client.add_device(dev, cfg[dev])
    #sleep(2)
    #client.sync_files(dev)
