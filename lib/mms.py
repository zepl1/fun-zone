from machine import I2C, Pin, unique_id
import neopixel

import os
import network
import binascii
import hashlib
import json
import gc

import time

# alternative for pinmap?
#from ucollections import namedtuple
from board import Board

from ads1115 import ADS1115
from mp8862 import MP8862

class MMS(Board):
    def __init__(self):
        super().__init__()

        with open('board.json','r') as f:
            cfg = json.load(f)

        self.name = cfg['name']
        self.mms = cfg['mms']
        self.heater_shunt = self.mms['heater_shunt']     # Ohm
        self.heater_offset = self.mms['heater_offset']  # V

        self.adc = ADS1115(self.i2c, gain=2) # gain 2 == +/- 4.096 V

        self.heater = MP8862(self.i2c, cfg['pins']['mp_en'], v_max=self.mms['heater_v_max'], i_max=self.mms['heater_i_max'])
        self.heater.set_voltage(0)
        self.heater.set_ilim(0.75)
        self.heater.update_status()

        self.data = { "time": None }
        for k in self.mms['channels'].keys():
            if k:
                self.data[k] = None

    def update_data(self):
        self.data['time'] = time.time()
        for k in self.mms['channels'].keys():
            self.data[k] = self.get_channel(self.mms['channels'][k])

    def get_channel(self, mch):
        if isinstance(mch['chan'], list):
            val = self.adc.raw_to_v(self.adc.read(rate=4, channel1=mch['chan'][0], channel2=mch['chan'][1]))
        else:
            val = self.adc.raw_to_v(self.adc.read(rate=4, channel1=mch['chan']))
        return val

    def send_data(self):
        print("DATA {}".format(json.dumps(self.data)))
