import urequests
import gc
import time
import json
import machine
rtc = machine.RTC()

def getTimeString(t = None):
    if t:
        now = t
    else:
        now = rtc.datetime()
    ms = '{:03}'.format(round(now[7]/1000))
    return "{}-{:02}-{:02} {:02}:{:02}:{:02}.{}".format(now[0], now[1], now[2], now[4],now[5], now[6], ms)

class GilgameshClient:
    def __init__(self, measurement, table, url='http://gilgamesh.ipc.uni-tuebingen.de:5555', in_mem=True):
        self.url = url
        self.measurement = measurement
        self.table = table
        self.upload_queue_filename = 'gm_upload_data.txt'
        self.upload_queue = []
        self.in_mem = in_mem
        self.data_is_new = True
        self.file_closed = False
        self.send_list =True
        self.init_data_file()


    def register_device(dbname):
        post_url = self.url + '/json/nupsert/'+str(doc['sensor_id']) + '/'+str(doc['tramp_name']).replace(' ','')

    def init_data_file(self):
        if self.in_mem:
            self.upload_queue = []
        else:
            if self.send_list:
                with open(self.upload_queue_filename, 'w') as f:
                    f.write('[')
                self.data_is_new = True
            else:
                with open(self.upload_queue_filename, 'w') as f:
                    f.write('')
                self.data_is_new = True
            self.file_closed = False

    def close_data_file(self):
        if self.file_closed:
            pass
        else:
            if self.send_list:
                with open(self.upload_queue_filename, 'a') as f:
                    f.write(']')
            else:
                pass
            self.file_closed = True


    def create_upload_doc(self, doc):
        tags = {}
        fields = {}
        for k in doc.keys():
            if type(doc[k])==str:
                tags[k] = doc[k]
            else:
                fields[k] = doc[k]
        upload_doc = {"fields":fields, "tags":tags}
        return upload_doc

    def add_to_upload_queue(self, doc, t=None):
        upload_doc = self.create_upload_doc(doc)
        t_str = getTimeString(t)
        upload_doc['time'] = t_str
        print('Added at ', t_str)
        if self.in_mem:
            self.upload_queue.append(upload_doc)
        else:
            if self.send_list:
                with open(self.upload_queue_filename, 'a') as f:
                    if self.data_is_new:
                        pass
                    else:
                        f.write(',')
                    f.write(json.dumps(upload_doc))
                    self.data_is_new = False
            else:
                with open(self.upload_queue_filename, 'a') as f:
                    if self.data_is_new:
                        pass
                    else:
                        f.write('\n')
                    f.write(json.dumps(upload_doc))
                    self.data_is_new = False

    def post_document(self, doc, measurement, table):
        gc.collect()
        post_url = self.url + '/series/insert/'+str(measurement) + '/'+str(table)
        print(post_url)
        upload_doc = self.create_upload_doc(doc)

        for i in range(4):
            print('GGM try #'+str(i))
            try:
                res = urequests.post(post_url, json=upload_doc, timeout=1)
                json_res = res.json()
                if json_res['Error']:
                    raise Exception('Error with Gilgamesh upload. Are all fields correct.', res)
                else:
                    print(json_res)
                    return True
            except:
                continue
        gc.collect()
        raise Exception('Gilgamesh upload error. Is the server running?')

    def post_raw_json(self, json_doc, measurement, table, retry=1):
        post_url = self.url + '/series/insert/'+str(measurement) + '/'+str(table)

        for i in range(retry):
            try:
                res = urequests.post(post_url, json=json_doc, timeout=0.5)
                json_res = res.json()
                if json_res['Error']:
                    print(json_res)
                    print(json_doc)
                    raise Exception('Error with Gilgamesh upload. Are all fields correct.', res)
                    print(json_res)
                else:
                    return True
            except Exception as e:
                print(e)
                print('GGM retry #'+str(i))
                continue
        gc.collect()
        raise Exception('Gilgamesh upload error. Is the server running?')


    def post_document_fromfile(self,filepath,  measurement, table):
        gc.collect()
        post_url = self.url + '/series/insert/'+str(measurement) + '/'+str(table)

        for i in range(4):
            try:
                res = urequests.post_file(post_url, data=filepath, timeout=15)
                json_res = res.json()
                if json_res['Error']:
                    print(json_res)
                    raise Exception('Error with Gilgamesh upload. Are all fields correct.', res)
                else:
                    return True
            except Exception as e:
                print(e)
                print('GGM retry #'+str(i))
                continue
        gc.collect()
        raise Exception('Gilgamesh upload error. Is the server running?')

    def flush_doc_queue(self,measurement, table):
        self.close_data_file()
        print('Start flushing local copy.')
        if self.in_mem:
            if len(self.upload_queue)>0:
                self.post_raw_json(json_doc=self.upload_queue, measurement=measurement, table=table)
        else:
            if self.send_list:
                self.post_document_fromfile(self.upload_queue_filename, measurement, table)
            else:
                with open(self.upload_queue_filename,'r') as f:
                    while True:
                        l = f.readline()
                        if l:
                            jline = json.loads(l)
                            self.post_raw_json(jline, measurement, table, retry=2)
                        else:
                            break
                    print('Saved data send.')
        print('Data send.')
        return True
        #self.init_data_file()
        gc.collect()

