from machine import I2C, Pin, unique_id, ADC
import neopixel

import os
import network
import binascii
import hashlib
import json
import gc

import time

# alternative for pinmap?
#from ucollections import namedtuple
from board import Board

class KK(Board):
    def __init__(self):
        super().__init__()

        with open('board.json','r') as f:
            cfg = json.load(f)

        self.name = cfg['name']

        # onboard adc inputs -- ATTN_11DB is ca. ~3.6V
        self.adc_bat = ADC(Pin(cfg['pins']['adc_bat']))
        self.adc_bat.atten(ADC.ATTN_11DB)
        self.adc_bat.width(ADC.WIDTH_11BIT)

        self.adc_sol = ADC(Pin(cfg['pins']['adc_sol']))
        self.adc_sol.atten(ADC.ATTN_11DB)
        self.adc_sol.width(ADC.WIDTH_11BIT)

        self.data = {
            "time": None ,
            "adc_bat": None,
            "adc_sol": None,
            "bat_charging": False
        }

    def get_charging_status(self):
        return False

    def update_data(self):
        self.data['time'] = time.ticks_ms()
        self.data['adc_bat'] = self.adc_bat.read()
        self.data['adc_sol'] = self.adc_sol.read()
        self.data['bat_charging'] = self.get_charging_status()

    def send_data(self):
        print("DATA {}".format(json.dumps(self.data)))
