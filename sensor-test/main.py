import json, sys, os, binascii, gc, hashlib
#from board import Board
from machine import Pin, TouchPad, I2C, SDCard
from mms import MMS as Board
from mp8862 import MP8862
import time

import ntptime

data_fname = '/sd/measurement-default.dat'

def update_fname():
    global data_fname
    data_fname = '/sd/'+str(time.time())+'.dat'

board = Board()
heater = MP8862(board.i2c, board.pins['mp_en'], v_max=7.0, i_max=1.5, heater_os=0.03)
heater.set_voltage(0)
heater.set_ilim(0.5)
heater.enable()

board.wifi_connect()
for x in range(3):
    try:
        ntptime.settime()
        break
    except:
        pass
update_fname()

board.set_led((9,0,9))
os.mount(SDCard(), "/sd")


def append_csv(data_dict):
    with open(data_fname, 'a') as f:
        f.write(json.dumps(data_dict))
        f.write('\n')
    print('saving data:', data_dict)

cnt = 0
NEW_FILECOUNT = 1000
heater.set_voltage(4.6)
while True:
    cnt += 1
    cnt %= NEW_FILECOUNT

    if cnt:
        pass
    else:
        update_fname()

    board.update_data()
    append_csv(board.data)
    time.sleep(1)
