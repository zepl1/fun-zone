import uctypes
import ubinascii
from machine import Pin
from time import sleep

class MP8862:
    """
    Not Implemented:
        ALT pin + interrupts
        mfg/dev id
        LDC, SS,  DISCHARGE, MODE, FREQ, ...
        ???????????????
        ????? OTP ?????
        ???????????????
    """
    def __init__(self, i2c, pin_en, address=0x6b, v_max=19.9, i_max=3.9, heater_os=0.03):
        """starts up with output disabled in software via i2c
        """
        self.i2c = i2c
        self.address = address
        self.pin_en = Pin(pin_en, Pin.OUT) # has internal pulldown

        # Limits
        self.v_max = v_max  # V
        self.i_max = i_max  # A

        self.heater_offset = heater_os # V

        # bitfields of interest --- 'R' == 'RESERVED'
        go_struct = {
                'GO_BIT':           uctypes.BFUINT8 | 0 | 0 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'PG_DELAY_EN':      uctypes.BFUINT8 | 0 | 1 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'R':                uctypes.BFUINT8 | 0 | 2 << uctypes.BF_POS | 6 << uctypes.BF_LEN,
        }

        ctl1_struct = {
                'R':                uctypes.BFUINT8 | 0 | 0 << uctypes.BF_POS | 2 << uctypes.BF_LEN,
                'FREQ':             uctypes.BFUINT8 | 0 | 2 << uctypes.BF_POS | 2 << uctypes.BF_LEN,
                'MODE':             uctypes.BFUINT8 | 0 | 4 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'DISCHG_EN':        uctypes.BFUINT8 | 0 | 5 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'HICCUP_OCP_OVP':   uctypes.BFUINT8 | 0 | 6 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'EN':               uctypes.BFUINT8 | 0 | 7 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
        }

        ctl2_struct = {
                'R':                uctypes.BFUINT8 | 0 | 0 << uctypes.BF_POS | 4 << uctypes.BF_LEN,
                'SS':               uctypes.BFUINT8 | 0 | 4 << uctypes.BF_POS | 2 << uctypes.BF_LEN,
                'LDC':              uctypes.BFUINT8 | 0 | 6 << uctypes.BF_POS | 2 << uctypes.BF_LEN,
        }

        status_struct = {
                'R':                uctypes.BFUINT8 | 0 | 0 << uctypes.BF_POS | 4 << uctypes.BF_LEN,
                'CC_CV':            uctypes.BFUINT8 | 0 | 4 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OTW':              uctypes.BFUINT8 | 0 | 5 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OTP':              uctypes.BFUINT8 | 0 | 6 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'PG':               uctypes.BFUINT8 | 0 | 7 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
        }

        interrupt_struct = {
                'PG_RISING':        uctypes.BFUINT8 | 0 | 0 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OTWARNING_EXIT':  uctypes.BFUINT8 | 0 | 1 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OTEMPP_EXIT':      uctypes.BFUINT8 | 0 | 2 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'UVP_FALLING':      uctypes.BFUINT8 | 0 | 3 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OC_RECOVER':       uctypes.BFUINT8 | 0 | 4 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OC_ENTER':         uctypes.BFUINT8 | 0 | 5 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OTWARNING_ENTER':  uctypes.BFUINT8 | 0 | 6 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OTEMPP_ENTER':     uctypes.BFUINT8 | 0 | 7 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
        }

        mask_struct = {
                'PG_MSK':           uctypes.BFUINT8 | 0 | 0 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'UVP_MSK':          uctypes.BFUINT8 | 0 | 1 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OC_MSK':           uctypes.BFUINT8 | 0 | 2 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OTWMSK':           uctypes.BFUINT8 | 0 | 3 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'OTPMSK':           uctypes.BFUINT8 | 0 | 4 << uctypes.BF_POS | 1 << uctypes.BF_LEN,
                'R':                uctypes.BFUINT8 | 0 | 5 << uctypes.BF_POS | 3 << uctypes.BF_LEN,
        }

        self.regs = {
            'VOUT_L':   { 'addr': 0x00 },
            'VOUT_H':   { 'addr': 0x01 },
            'VOUT_GO':  { 'addr': 0x02, 'struct': go_struct },
            'IOUT_LIM': { 'addr': 0x03 },
            'CTL1':     { 'addr': 0x04, 'struct': ctl1_struct },
            'CTL2':     { 'addr': 0x05, 'struct': ctl2_struct },
            'R0':       { 'addr': 0x06 },
            'R1':       { 'addr': 0x07 },
            'R2':       { 'addr': 0x08 },
            'STATUS':   { 'addr': 0x09, 'struct': status_struct },
            'INTERRUPT':{ 'addr': 0x0A, 'struct': interrupt_struct },
            'MASK':     { 'addr': 0x0B, 'struct': mask_struct },
            'ID1':      { 'addr': 0x0C },
            'MFR_ID':   { 'addr': 0x27 },
            'DEV_ID':   { 'addr': 0x28 },
            'IC_REV':   { 'addr': 0x29 }
        }

        self.enable(soft=False)
        self.update_status()
        self.disable()
        self.update_status()

    def enable(self, soft=True):
        """enables output via i2c(default) or EN-Pin
        """
        if soft:
            ctl1 = self.get_struct(self.regs['CTL1']['val'], self.regs['CTL1']['struct'])
            ctl1.EN = 1
            self.i2c.writeto_mem(self.address, self.regs['CTL1']['addr'], bytes(ctl1))
        else:
            self.pin_en.value(1)

    def disable(self, soft=True):
        """disables output via i2c(default) or EN-Pin
        """
        if soft:
            ctl1 = self.get_struct(self.regs['CTL1']['val'], self.regs['CTL1']['struct'])
            ctl1.EN = 0
            self.i2c.writeto_mem(self.address, self.regs['CTL1']['addr'], bytes(ctl1))
        else:
            self.pin_en.value(0)
        pass

    def set_voltage(self, volts):
        v = int(min(max(volts-self.heater_offset, 0), self.v_max)*100)
        v_l = 0x7 & v
        v_h = v >> 3
        self.i2c.writeto_mem(self.address, self.regs['VOUT_L']['addr'], bytes([v_l]))
        self.i2c.writeto_mem(self.address, self.regs['VOUT_H']['addr'], bytes([v_h]))
        # FIXME PG_DELAY_EN always written to 0
        go = self.get_struct(self.regs['VOUT_GO']['val'], self.regs['VOUT_GO']['struct'])
        go.GO_BIT = 1
        self.i2c.writeto_mem(self.address, self.regs['VOUT_GO']['addr'], bytes(go))

    def get_voltage(self):
        volts = (0x7 & self.regs['VOUT_L']['val']) |  self.regs['VOUT_H']['val'] << 3
        return volts/100

    def set_ilim(self, amps):
        """setting ilim should be done in steps
        """
        val = int(min(max(amps, 0), self.i_max)/0.05)
        oldval = 0x7f & self.regs['IOUT_LIM']['val']
        if oldval < val:
            step = 1
        else:
            step = -1

        for i in range(oldval, val+step, step):
            self.i2c.writeto_mem(self.address, self.regs['IOUT_LIM']['addr'], bytes([i]))
            sleep(0.05)

    def get_ilim(self):
        amps = 0x7f & self.regs['IOUT_LIM']['val']
        return amps*0.05

    def update_regs(self):
        """ make a shadow copy of the device registers
        """
        buf = bytearray(1)
        for reg in self.regs.keys():
            self.i2c.readfrom_mem_into(self.address, self.regs[reg]['addr'], buf)
            self.regs[reg]['val'] = int.from_bytes(buf, 'little')

    def update_status(self, debug=False):
        """ keeps a hashmap of interesting status bits
        """
        self.update_regs()

        ctl1 = self.get_struct(self.regs['CTL1']['val'], self.regs['CTL1']['struct'])
        go = self.get_struct(self.regs['VOUT_GO']['val'], self.regs['VOUT_GO']['struct'])
        stat = self.get_struct(self.regs['STATUS']['val'], self.regs['STATUS']['struct'])

        self.status = {
            'V_OUT': self.get_voltage(),
            'I_LIM': self.get_ilim(),
            'EN': True if (self.pin_en.value() and ctl1.EN) else False,
            'STATUS': {
                'PG': stat.PG,
                'OTP': stat.OTP,
                'OTW': stat.OTW,
                'CC_CV': stat.CC_CV,
            },
            'CTL': {
                'EN': ctl1.EN,
                'GPIO_EN': self.pin_en.value(),
                'VOUT_GO': go.GO_BIT,
            }
        }

        if debug:
            ctl2 = self.get_struct(self.regs['CTL2']['val'], self.regs['CTL2']['struct'])
            mask = self.get_struct(self.regs['MASK']['val'], self.regs['MASK']['struct'])
            itr = self.get_struct(self.regs['INTERRUPT']['val'], self.regs['INTERRUPT']['struct'])

            print('=== VOUT_GO ===')
            #print('{:08b}'.format(self.regs['CTL2']['val']))
            print('GO_BIT {} - PG_DELAY_EN {}'.format(
                go.GO_BIT, go.PG_DELAY_EN))

            print('=== CTL1 ===')
            #print('{:08b}'.format(self.regs['CTL1']['val']))
            print('EN {} - HICCUP_OCP_OVP, {} - DISCHG_EN {} - MODE {} - FREQ {}'.format(
                ctl1.EN, ctl1.HICCUP_OCP_OVP, ctl1.DISCHG_EN, ctl1.MODE, ctl1.FREQ))

            print('=== CTL2 ===')
            #print('{:08b}'.format(self.regs['CTL2']['val']))
            print('LDC {} - SS {}'.format(
                ctl2.LDC, ctl2.SS))

            print('=== STATUS ===')
            #print('{:08b}'.format(self.regs['STATUS']['val']))
            print('pg {} - otp {} - otw {} - cc_cv {}'.format(stat.PG, stat.OTP, stat.OTW, stat.CC_CV))

            print('=== MASK ===')
            #print('{:08b}'.format(self.regs['MASK']['val']))
            print('OPTMSK {} - OTWMSK {} - OC_MSK {} - UVP_MSK {} - PG_MSK {}'.format(
                mask.OTPMSK, mask.OTWMSK, mask.OC_MSK, mask.UVP_MSK, mask.PG_MSK))

            print('=== INTERRUPT ===')
            #print('{:08b}'.format(self.regs['INTERRUPT']['val']))
            print('OTEMPP_ENTER {} - OTWARNING_ENTER {} - OC_ENTER {} - OC_RECOVER {} - UVP_FALLING {} - OTEMPP_EXIT {} - OTWARNING_EXIT {} - PG_RISING {}'.format(
                itr.OTEMPP_ENTER,  itr.OTWARNING_ENTER, itr.OC_ENTER, itr.OC_RECOVER, itr.UVP_FALLING, itr.OTEMPP_EXIT, itr.OTWARNING_EXIT, itr.PG_RISING,))

            print(self.status)

    def get_struct(self, val, struct):
        return uctypes.struct(uctypes.addressof(bytes([val])), struct)

    def reset_interrupts(self):
        self.i2c.writeto_mem(self.address, self.regs['INTERRUPT']['addr'], b'0xFF')
