from machine import I2C, Pin, unique_id
import neopixel

import os
import network
import binascii
import hashlib
import json
import gc

import time

# alternative for pinmap?
#from ucollections import namedtuple

class Board:
    """
    This is a generic Board class for esp32 based boards with
    a neopixel attached to it and i2c bus configured, sd card is optional
    """
    def __init__(self):
        self.dev_mac = binascii.hexlify(unique_id()).decode()

        with open('board.json','r') as f:
            board = json.load(f)

        self.name = board['name']
        self.devices = board['i2c']
        self.sdcard = board['sdcard']

        #pins = board['pins']
        #for p in pins.keys():
        #    pins[p] = Pin(pins[p])
        #self.pins = pins
        self.pins = board['pins']

        self.led = neopixel.NeoPixel(Pin(self.pins['led']), 1)
        self.i2c = I2C(0, scl=Pin(self.pins['scl']), sda=Pin(self.pins['sda']), freq=400000)

    def set_led(self, val=(0,0,0)):
        try:
            self.led.fill(val)
            self.led.write()
        except:
            print("error setting led")

    def get_hash(self, d, f):
        fname = d+f
        with open(fname, 'r') as fd:
            hasher = hashlib.sha1()
            lines = fd.readlines()
            hasher.update("".join(lines))
            fhash = binascii.hexlify(hasher.digest())
        return fhash

    def update_hashtab(self):
        """FIXME not a nice way to handle this
        but it works for now...
        """
        file_hashtab = {}
        DIR_BIT = 0x4000
        FILE_BIT = 0x8000
        CWD = '/'
        os.chdir(CWD)
        for f in os.ilistdir():
            fname = f[0]
            ftype = f[1]
            if not ftype == FILE_BIT:
                continue
            file_hashtab[fname] = self.get_hash(CWD, fname)

        if not 'lib' in os.listdir(CWD):
            os.mkdir('lib')
        for f in os.ilistdir('/lib'):
            fname = f[0]
            ftype = f[1]
            if not ftype == FILE_BIT:
                continue
            file_hashtab['/lib/'+fname] = self.get_hash('/lib/', fname)

        self.f_hashtab = json.dumps(file_hashtab)
        del file_hashtab
        gc.collect()

    def scan_good_wifi(self, sta_if, wifi_configs):
        wifi_scan = sta_if.scan()
        wifi_scan =  [[w[0].decode(), binascii.hexlify(w[1]).decode(),w[2],w[3],w[4],w[5]] for w in wifi_scan]
        wifi_scan_sorted = sorted(wifi_scan, key=lambda e:e[3], reverse=True)
        good_wifis = [[w[0],wifi_configs[w[0]]] for w in wifi_scan_sorted if (w[0] in list(wifi_configs.keys()))]
        return good_wifis

    def wifi_connect(self, timeout = 10):
        try:
            with open('wifi.secret','r') as f:
                wifi_configs = json.load(f)
        except:
            print("Error loading wifi secrets!")

        sta_if = network.WLAN(network.STA_IF)
        sta_if.active(True)
        if not sta_if.isconnected():
            print('connecting to network...')
            good_wifis = self.scan_good_wifi(sta_if, wifi_configs)
            for good_wifi in good_wifis:
                sta_if.connect(good_wifi[0], good_wifi[1])
                start = time.time()
                while not sta_if.isconnected():
                    time.sleep(0.2)
                    if (time.time()-start)>timeout:
                        print("Timeout connecting to wifi!")
                        break
                if sta_if.isconnected():
                    print('network config:', sta_if.ifconfig())
                    break

        self.wifi = sta_if

    def wifi_disable(self):
        self.wifi.disconnect()
        self.wifi.active(False)
