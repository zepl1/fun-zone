import sys, select
from mms import MMS
import json

from time import sleep

p = select.poll()
p.register(sys.stdin, select.POLLIN)

mms = MMS()
#print("LOG Heater Status: {}".format(mms.heater.status))
mms.send_data()
#mms.heater.enable()

ctrl_input = ''

def parse_input():
    global ctrl_input
    if ctrl_input and ctrl_input[-1] == '\n':
        try:
            cmd = ctrl_input.split(' ', 1)
            if cmd[0] == 'CTRL':
                cmd = json.loads(cmd[1])
                single_cmd(cmd)
                #channel_cmd(cmd)
            else:
                print('LOG Invalid command received:', cmd)
        except Exception as e:
            print('ERROR Malformed command received:', e)
        ctrl_input = ''
    else:
        #print('need moar input!')
        pass

def single_cmd(cmd):
    v = cmd['voltage']+mms.heater_offset
    if v == 0:
        mms.heater.disable()
    else:
        mms.heater.enable()
        mms.heater.set_voltage(v)

    mms.heater.update_status()
    print('LOG ', mms.heater.status)

def channel_cmd(cmd):
    """TODO...not implemented...
    """
    for ch in cmd.keys():
        mms.channel.set(cmd[ch])

while(True):
    while p.poll(100):
        ctrl_input += sys.stdin.read(1)
        parse_input()
    mms.update_data()
    mms.send_data()
    #sleep(1)
